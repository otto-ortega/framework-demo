exports.login_flow = 
{   
    url : "https://www.aetna.com/provweb/login.fcc",
    wait: 
    {
        condition: "networkidle0",
        timeout  : 30
    },
    verify:
    {
        selector : 'input[name="USER"]'
    },
    actions:
    [
        {action: "type"  , selector: 'input[name="USER"]'    , value: "user"},
        { action: "wait" , value : 2 },
        {action: "type"  , selector: 'input[name="PASSWORD"]', value: "password"},
        { action: "wait" , value : 2 },
        {action: "click" , selector: 'input[value="Log In"]'  }
    ]
};