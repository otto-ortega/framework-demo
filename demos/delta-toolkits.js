exports.login_flow = 
{   
    url : "https://www.dentalofficetoolkit.com/dot-ui/login",
    wait: 
    {
        condition: "networkidle0",
        timeout  : 30
    },
    verify:
    {
        selector : "#secureauth_hosted_pgid"
    },
    actions:
    [
        { action: "wait"     , value : 5 },
        { action: "press"    , value : "Tab" },
        { action: "keyboard" , value : "monarchcl12" },
        { action: "wait"     , value : 2 },
        { action: "press"    , value : "Tab" },
        { action: "keyboard" , value : "smiles2012" },
        { action: "wait"     , value : 2 },
        { action: "press"    , value : "Tab" },
        { action: "press"    , value : "Enter" },
    ]
};



exports.search_office = 
{
    actions:
    [
        { action: "wait"     , selector : ".change-office" },
        { action: "wait"     , value    : 5 },
        { action: "click"    , selector : ".change-office" },
        { action: "type"     , selector : 'input[name="dentist-name"]', value: "dawes" }
    ]
};

exports.set_zipcode = 
{
    actions:
    [
        { action: "type"     , selector : 'input[name="dentist-address"]', value: "44109" }
    ]
};

exports.select_office = 
{
    actions:
    [
        { action: "wait"     , value    : 3 },
        { action: "click"    , selector : '.provider-results ul li:nth-child(1)'},
        { action: "wait"     , value    : 3 }
    ]
};


exports.search_member = 
{
    actions:
    [
        { action: "click"    , selector : 'app-member-header button[type="submit"]' },
        { action: "type"     , selector : 'input[name="member_id"]', value: "913201712" },
        { action: "wait"     , value    : 3 },
        { action: "click"    , selector : 'button.member-search' },
    ]
};


exports.claim_search = 
{   
    url : "https://www.dentalofficetoolkit.com/dot-ui/search",
    wait: 
    {
        condition: "networkidle0",
        timeout  : 30
    },
    verify:
    {
        selector : "button.submit"
    },
    actions:
    [
        { action: "click"    , selector: "button.submit"},
        { action: "wait"     , value   : 5 }
    ]
};
