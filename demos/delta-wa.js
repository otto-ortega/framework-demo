exports.login_flow = 
{   
    url : "https://www2.deltadentalwa.com/provider",
    wait: 
    {
        condition: "networkidle0",
        timeout  : 30
    },
    verify:
    {
        selector : "#headerBar_headerBarSignInLink"
    },
    actions:
    [
        {action: "click" , selector: "#headerBar_headerBarSignInLink"},
        {action: "type"  , selector: "#headerBar_headerBarSignIn_tbUsername", value: "user"},
        {action: "type"  , selector: "#headerBar_headerBarSignIn_tbPassword", value: "password"},
        {action: "press" , value   : "Tab"},
        {action: "press" , value   : "Enter"}
    ]
};