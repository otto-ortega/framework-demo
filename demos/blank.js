exports.login_flow = 
{   
    url : "",
    wait: 
    {
        condition: "load",
        timeout  : 30
    },
    verify: 
    {
        selector: "",
        xpath   : "",
        regex   : "",
        timeout : 90
    },
    actions:
    [
        {action: "click"    , selector: ""},
        {action: "type"     , selector: "", value: ""},
        {action: "press"    , value: ""},
        {action: "keyboard" , value: ""},
        {action: "scroll"   , selector: ""},
    ]
};