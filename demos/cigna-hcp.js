exports.login_flow = 
{   
    url : "https://cignaforhcp.cigna.com/app/login",
    wait: 
    {
        condition: "networkidle0",
        timeout  : 30
    },
    verify:
    {
        selector : "#username"
    },
    actions:
    [
        {action: "type"  , selector: "#username", value: "BND2018"},
        {action: "type"  , selector: "#password", value: "Mysmiles2020"},
        {action: "click" , selector: 'input[value="Log In"]'},
    ]
};