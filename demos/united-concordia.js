exports.login_flow = 
{   
    url : "https://www.unitedconcordia.com/dental-insurance/dentist/",
    wait: 
    {
        condition: "networkidle0",
        timeout  : 30
    },
    verify:
    {
        selector : 'input[name="signinForm:username"]'
    },
    actions:
    [
        {action: "type"  , selector: 'input[name="signinForm:username"]', value: "smilesAR"},
        {action: "type"  , selector: 'input[name="signinForm:password"]', value: "Every1smiles!"},
        {action: "click" , selector: 'a[name="signinForm:submit"]'},
    ]
};