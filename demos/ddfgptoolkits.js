exports.login_flow = 
{   
    url : "https://www.ddfgptoolkits.com/dotWeb/appmanager/dot/desktop",
    wait: 
    {
        condition: "networkidle0",
        timeout  : 30
    },
    verify:
    {
        selector : "#userName"
    },
    actions:
    [
        {action: "type"  , selector: "#userName"     , value: "WAbnd10321"},
        {action: "type"  , selector: "#inputPassword", value: "Smiles4all"},
        {action: "click" , selector: 'input[alt="login!"]'},
    ]
};



exports.search_office = 
{
    wait: 
    {
        condition: "networkidle0"
    },
    actions:
    [
        { action: "wait"     , selector : ".change-office" },
        { action: "wait"     , value    : 5 },
        { action: "click"    , selector : ".change-office" },
        { action: "type"     , selector : 'input[name="dentist-name"]', value: "dawes" }
    ]
};



exports.set_zipcode = 
{
    actions:
    [
        { action: "type"     , selector : 'input[name="dentist-address"]', value: "44109" }
    ]
};

exports.select_office = 
{
    actions:
    [
        { action: "click"     , selector : '.provider-results ul li:nth-child(1)'}
    ]
};