exports.login_flow = 
{   
    url : "https://login-wsprod.deltadental.com/generic_signin.jsp?sourceid=nedelta",
    wait: 
    {
        condition: "networkidle0",
        timeout  : 30
    },
    verify:
    {
        selector : "#login-username"
    },
    actions:
    [
        {action: "type"  , selector: "#login-username", value: "BND2018"},
        {action: "type"  , selector: "#login-password", value: "Mysmiles2020"},
        {action: "click" , selector: 'input[value="Login"]'},
    ]
};