/*jshint esversion: 8 */

const collector = require("@otto-ortega/collector-framework");
const website   = require("./demos/united-concordia.js");

(
    async () =>
    {
        try
        {
         
            collector.show_browser = true;
            
            await collector.initialize();

            let batchData  = await collector.loadBach();

            console.log(batchData);

            process.exit();

            await collector.navigate(website.login_flow);

            /*
            console.log("* Change Office:\n");

            await collector.navigate(website.search_office);

            console.log("* Change Member:\n");

            await collector.navigate(website.search_member);

            console.log("* Claim Search:\n");

            await collector.navigate(website.claim_search);

            let fetchedData = collector.fetchAjaxResponse("https://www.dentalofficetoolkit.com/api/dot-gateway/v1/provider/search?userId=monarchcl12&lastName=dawes&address=44109");

            await collector.navigate(website.set_zipcode);

            let officeList  = await fetchedData;

            console.log("* Office list:\n");

            console.log( JSON.stringify(officeList));

            await collector.navigate(website.select_office);
            */

        }
        catch(e)
        {
            switch(e.type)
            {
                case "InvalidCredentials":
                break;
        
                case "NetworkError":
                break;
        
                case "FlowBroken":
                break;
            }
        }
    }
)();

